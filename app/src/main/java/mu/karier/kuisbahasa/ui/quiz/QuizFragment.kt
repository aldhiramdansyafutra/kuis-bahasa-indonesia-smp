package mu.karier.kuisbahasa.ui.quiz

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialog
import mu.karier.kuisbahasa.Application
import mu.karier.kuisbahasa.R
import mu.karier.kuisbahasa.data.Questions
import mu.karier.kuisbahasa.databinding.LayoutQuestionBinding
import mu.karier.kuisbahasa.databinding.LayoutQuestionResultBinding
import kotlin.math.roundToInt


/**
 *
 * Validation correct answer [done]
 * Populate score [done]
 * PopUp next question [done]
 * QuizResultFragment
 * LeaderboardFragment
 */
class QuizFragment : Fragment() {

    private var _binding: LayoutQuestionBinding? = null
    private val binding get() = _binding!!

    private val viewModel: QuizViewModel by viewModels {
        QuizViewModelFactory((requireActivity().application as Application).repository)
    }

    private var handler: CountDownTimer? = null

    private var questions = ArrayList<Questions>()
    private var currentIndex = -1

    private var scorePerItem = 0
    private var currentScore = 0

    private var currentQuestion = Questions()

    private var username = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LayoutQuestionBinding.inflate(inflater, container, false)
        questions = Questions.getQuestions(requireContext())
        scorePerItem = 100 / questions.size
        arguments?.let {
            username = it.getString("username", "")
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnNext.setOnClickListener {
            if (handler != null) handler?.cancel()
            validateSelectedAnswer()
        }
        start()
    }

    override fun onDestroy() {
        super.onDestroy()
        clear()
    }

    private fun start() {
        currentIndex += 1
        currentQuestion = questions[currentIndex]

        binding.apply {
            tvQuestion.text = currentQuestion.question
            tvQuestionCount.text = "Pertanyaan ke-${currentIndex + 1}"
        }
        addAnswer(currentQuestion.getAnswers())
        setupTimer(currentQuestion.duration, ::validateSelectedAnswer)
    }

    private fun nextQuestion() {
        if (currentIndex in -1 until questions.size - 1) {
            clear()
            start()
        } else {
            this.findNavController()
                .navigate(
                    QuizFragmentDirections.actionQuizToQuizResult(
                        username,
                        currentScore.toString()
                    )
                )
        }
    }

    private fun setupTimer(duration: Long, doSomething: () -> Unit) {
        handler = object : CountDownTimer(duration, 1000) {
            override fun onTick(milisecond: Long) {
                val second = milisecond / 1000
                binding.tvTimer.text = "00:0$second"
            }

            override fun onFinish() {
                doSomething()
            }
        }
        handler?.start()
    }

    private fun addAnswer(answers: List<String>) {
        binding.radioGroup.removeAllViews()
        answers.forEach { answer ->
            val button = RadioButton(requireContext())
            button.id = View.generateViewId()
            button.text = answer.trim()
            binding.radioGroup.addView(button)
        }
    }

    private fun validateSelectedAnswer() {
        val id = binding.radioGroup.checkedRadioButtonId
        val correctAnswer = currentQuestion.correctAnswer.trim()
        val currentAnswer: String =
            (view?.findViewById<RadioButton>(id)?.text ?: "").toString().trim()
        val isCorrect = correctAnswer.equals(currentAnswer, true)
        if (isCorrect) currentScore += scorePerItem
        Log.d("Current Answer >>>>>", currentAnswer)
        Log.d("Correct Answer >>>>>", correctAnswer)

        val score = currentScore.toDouble().roundToInt()
        binding.tvScore.text = "Skor: $score"
        showBottomSheet(isCorrect)
    }

    private fun clear() {
        if (handler != null) handler?.cancel()
        binding.radioGroup.removeAllViews()
    }

    private fun showBottomSheet(isCorrect: Boolean) {
        val bottomSheet = BottomSheetDialog(requireContext())
        val viewBinding = LayoutQuestionResultBinding.inflate(
            layoutInflater, binding.container, false
        )

        var imageResult = R.drawable.ic_thumb_up
        var message =
            "Yeaayy jawaban kamu tepat, kamu pasti bisa menjawab pertanyaan selanjutnya. Semangaat!!"

        if (!isCorrect) {
            imageResult = R.drawable.ic_thumb_down
            message =
                "Oops sepertinya jawaban kamu belum tepat, jagan menyerah yuk kamu pasti bisa!"
        }

        val isLastQuestions = currentIndex !in -1 until questions.size - 1
        if (isLastQuestions) {
            imageResult = R.drawable.ic_correct
            message = "Kamu telah menyelesaikan semua pertanyaan, yuk kita lihat hasilnya"
        }

        viewBinding.ivResult.setImageResource(imageResult)
        viewBinding.tvStatus.text = message
        viewBinding.btnNextQuestion.setOnClickListener {
            bottomSheet.dismiss()
        }
        bottomSheet.setOnDismissListener {
            nextQuestion()
        }
        bottomSheet.setContentView(viewBinding.root)
        bottomSheet.show()
    }
}