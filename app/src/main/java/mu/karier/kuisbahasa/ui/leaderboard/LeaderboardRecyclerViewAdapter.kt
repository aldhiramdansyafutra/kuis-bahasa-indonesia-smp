package mu.karier.kuisbahasa.ui.leaderboard

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import mu.karier.kuisbahasa.R
import mu.karier.kuisbahasa.database.entity.LeaderboardEntity
import mu.karier.kuisbahasa.databinding.ItemLeaderboardBinding

class LeaderboardRecyclerViewAdapter(
    private var values: List<LeaderboardEntity>
) : RecyclerView.Adapter<LeaderboardRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLeaderboardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        val trophy: Int = when (position) {
            0 -> R.drawable.ic_gold_trophy
            1 -> R.drawable.ic_silver_trophy
            2 -> R.drawable.ic_bronze_trophy
            else -> R.drawable.ic_medal
        }
        holder.binding.apply {
            tvNumber.text = (position + 1).toString()
            ivTrophy.setImageResource(trophy)
            tvUsername.text = item.username
            tvScore.text = item.score.toString()
            ivTrophy.isVisible = trophy > 0
        }
    }

    override fun getItemCount(): Int = values.size

    @SuppressLint("NotifyDataSetChanged")
    fun addItems(leaderboards: List<LeaderboardEntity>) {
        values = leaderboards
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: ItemLeaderboardBinding) :
        RecyclerView.ViewHolder(binding.root)

}