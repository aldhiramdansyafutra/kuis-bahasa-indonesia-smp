package mu.karier.kuisbahasa.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import mu.karier.kuisbahasa.data.Materi
import mu.karier.kuisbahasa.databinding.FragmentItemMateriBinding
import mu.karier.kuisbahasa.extension.openChromeBrowser

/**
 * A fragment representing a list of Items.
 */
class MateriFragment : Fragment() {

    var materiAdapter: MateriRecyclerViewAdapter? = null
    private var _binding: FragmentItemMateriBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentItemMateriBinding.inflate(inflater, container, false)
        materiAdapter = MateriRecyclerViewAdapter(Materi.getMateri(requireContext()))
        materiAdapter?.materiAdapterListener = object : MateriAdapterListener {
            override fun onMateriClicked(materi: Materi) {
                openChromeBrowser(materi.url)
            }
        }

        // Set the adapter
        with(binding.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = materiAdapter
        }
        return binding.root
    }
}