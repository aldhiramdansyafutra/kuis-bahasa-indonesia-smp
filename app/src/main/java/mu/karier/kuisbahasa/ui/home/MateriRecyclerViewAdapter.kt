package mu.karier.kuisbahasa.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mu.karier.kuisbahasa.data.Materi
import mu.karier.kuisbahasa.databinding.FragmentMateriBinding

class MateriRecyclerViewAdapter(
    private val values: List<Materi>
) : RecyclerView.Adapter<MateriRecyclerViewAdapter.ViewHolder>() {

    var materiAdapterListener: MateriAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentMateriBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.tvName.text = item.name
        holder.binding.tvDescription.text = item.description
        holder.itemView.setOnClickListener { materiAdapterListener?.onMateriClicked(item) }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: FragmentMateriBinding) :
        RecyclerView.ViewHolder(binding.root)

}

interface MateriAdapterListener {
    fun onMateriClicked(materi: Materi)
}