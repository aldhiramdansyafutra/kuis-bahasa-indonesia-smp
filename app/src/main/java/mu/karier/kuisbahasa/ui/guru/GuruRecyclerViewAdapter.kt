package mu.karier.kuisbahasa.ui.guru

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import mu.karier.kuisbahasa.R
import mu.karier.kuisbahasa.data.Guru
import mu.karier.kuisbahasa.databinding.FragmentItemGuruBinding

class GuruRecyclerViewAdapter(
    private val values: List<Guru>
) : RecyclerView.Adapter<GuruRecyclerViewAdapter.ViewHolder>() {

    var materiAdapterListener: GuruAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentItemGuruBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.tvName.text = item.nama
        holder.binding.tvStudy.text = "Bidang Studi ${item.studi}"
        holder.binding.ivAvatar.load(item.avatar) {
            transformations(CircleCropTransformation())
            crossfade(true)
            placeholder(R.drawable.bg_item_materi)
        }
        holder.itemView.setOnClickListener { materiAdapterListener?.onClicked(item) }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: FragmentItemGuruBinding) :
        RecyclerView.ViewHolder(binding.root)

}

interface GuruAdapterListener {
    fun onClicked(materi: Guru)
}