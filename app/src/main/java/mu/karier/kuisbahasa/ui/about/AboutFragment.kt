package mu.karier.kuisbahasa.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import mu.karier.kuisbahasa.data.Faq
import mu.karier.kuisbahasa.databinding.FragmentAboutBinding

/**
 * A fragment representing a list of Items.
 */
class AboutFragment : Fragment() {

    private var _binding: FragmentAboutBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAboutBinding.inflate(inflater, container, false)

        with(binding.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = AboutRecyclerViewAdapter(Faq.getFaqs(requireContext()))
        }
        return binding.root
    }

}