package mu.karier.kuisbahasa.ui.leaderboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import mu.karier.kuisbahasa.Application
import mu.karier.kuisbahasa.database.entity.LeaderboardEntity
import mu.karier.kuisbahasa.databinding.LeaderboardFragmentBinding
import mu.karier.kuisbahasa.ui.quiz.QuizViewModel
import mu.karier.kuisbahasa.ui.quiz.QuizViewModelFactory

/**
 * A fragment representing a list of Items.
 */
class LeaderboardFragment : Fragment() {

    private var _binding: LeaderboardFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: QuizViewModel by viewModels {
        QuizViewModelFactory((requireActivity().application as Application).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LeaderboardFragmentBinding.inflate(inflater, container, false)
        with(binding.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = LeaderboardRecyclerViewAdapter(ArrayList())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.mAllRecord.observe(viewLifecycleOwner, {
            val first = it[0]
            binding.containerHeader.tvUsername.text = first.username
            binding.containerHeader.tvScore.text = first.score.toString()
            (binding.list.adapter as LeaderboardRecyclerViewAdapter).addItems(it)
        })
    }
}