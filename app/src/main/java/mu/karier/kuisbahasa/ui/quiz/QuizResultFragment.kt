package mu.karier.kuisbahasa.ui.quiz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mu.karier.kuisbahasa.Application
import mu.karier.kuisbahasa.R
import mu.karier.kuisbahasa.databinding.FragmentQuizResultBinding
import mu.karier.kuisbahasa.databinding.LayoutQuestionBinding
import mu.karier.kuisbahasa.ui.guru.GalleryViewModel

class QuizResultFragment : Fragment() {

    private val viewModel: QuizViewModel by viewModels {
        QuizViewModelFactory((requireActivity().application as Application).repository)
    }

    private var _binding: FragmentQuizResultBinding? = null
    private val binding get() = _binding!!

    private var username = ""
    private var skor = "0"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentQuizResultBinding.inflate(inflater, container, false)
        arguments?.let {
            username = it.getString("username", "")
            skor = it.getString("skor", "0")
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            tvSkor.text = skor
            tvCaption.text = getString(R.string.quiz_result, username)
            btnFinish.setOnClickListener {
                viewModel.insert(username, skor)
                findNavController().popBackStack()
            }
            btnLeaderboard.setOnClickListener {
                viewModel.insert(username, skor)
                findNavController().navigate(QuizResultFragmentDirections.actionQuizResultToLeaderboard())
            }
        }
    }
}