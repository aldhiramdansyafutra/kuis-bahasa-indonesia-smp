package mu.karier.kuisbahasa.ui.guru

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import mu.karier.kuisbahasa.data.Guru
import mu.karier.kuisbahasa.databinding.BottomSheetCallConfirmationBinding
import mu.karier.kuisbahasa.databinding.FragmentItemMateriBinding
import android.content.Intent
import android.net.Uri
import mu.karier.kuisbahasa.extension.openChromeBrowser
import android.widget.Toast

import android.content.pm.PackageManager

import android.content.pm.PackageInfo


/**
 * A fragment representing a list of Items.
 */
class GuruFragment : Fragment() {

    var guruAdapter: GuruRecyclerViewAdapter? = null
    private var _binding: FragmentItemMateriBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentItemMateriBinding.inflate(inflater, container, false)
        guruAdapter = GuruRecyclerViewAdapter(Guru.getGuru(requireContext()))
        guruAdapter?.materiAdapterListener = object : GuruAdapterListener {
            override fun onClicked(materi: Guru) {
                showBottomSheet(materi)
            }
        }

        // Set the adapter
        with(binding.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = guruAdapter
        }
        return binding.root
    }

    private fun showBottomSheet(guru: Guru) {
        val bottomSheet = BottomSheetDialog(requireContext())
        val viewBinding = BottomSheetCallConfirmationBinding.inflate(
            layoutInflater, binding.container, false
        )
        viewBinding.tvCallTeacher.text = "Hubungi Bapak/Ibu ${guru.nama}?"
        viewBinding.btnPhone.setOnClickListener {
            bottomSheet.dismiss()
            phoneCall(guru.phone)
        }
        viewBinding.btnText.setOnClickListener {
            bottomSheet.dismiss()
            openWhatsapp(guru)
        }
        bottomSheet.setContentView(viewBinding.root)
        bottomSheet.show()
    }

    private fun phoneCall(phone: String) {
        val uri = "tel:" + phone.trim()
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(uri)
        startActivity(intent)
    }

    private fun openWhatsapp(guru: Guru) {
        if (isWhatsAppInstalled()) {
            val text =
                "Halo Bapak/Ibu ${guru.nama}, saya ingin bertanya tentang ${guru.studi}. Apakah Bapak/Ibu dapat membantu saya?"
            openChromeBrowser("https://wa.me/${guru.phone}?text=${text.replace(text, "%20", true)}")
        } else {
            Toast.makeText(
                requireContext(),
                "Kamu tidak memiliki aplikasi whatsapp",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun isWhatsAppInstalled(): Boolean {
        val pm: PackageManager = requireActivity().packageManager
        return try {
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }
}