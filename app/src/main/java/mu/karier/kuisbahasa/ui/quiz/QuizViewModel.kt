package mu.karier.kuisbahasa.ui.quiz

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import mu.karier.kuisbahasa.database.entity.LeaderboardEntity
import mu.karier.kuisbahasa.database.entity.LeaderboardRepository

class QuizViewModel(private val mRepository: LeaderboardRepository) : ViewModel() {

    val mAllRecord: LiveData<List<LeaderboardEntity>> = mRepository.mAllLeaderboard.asLiveData()

    fun insert(username: String, score: String) = viewModelScope.launch {
        mRepository.insert(LeaderboardEntity(username = username, score = score.toInt()))
    }

}

class QuizViewModelFactory(private val repository: LeaderboardRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QuizViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return QuizViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}