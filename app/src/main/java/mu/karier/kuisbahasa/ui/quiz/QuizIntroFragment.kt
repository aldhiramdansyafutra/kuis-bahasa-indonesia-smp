package mu.karier.kuisbahasa.ui.quiz

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialog
import mu.karier.kuisbahasa.databinding.BottomSheetStartQuizBinding
import mu.karier.kuisbahasa.databinding.FragmentQuizIntroBinding

class QuizIntroFragment : Fragment() {

    private var _binding: FragmentQuizIntroBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentQuizIntroBinding.inflate(inflater, container, false)
        binding.btnStart.setOnClickListener { showBottomSheet() }
        binding.btnLeaderboard.setOnClickListener {
            this.findNavController().navigate(
                QuizIntroFragmentDirections.actionIntroToLeaderboard()
            )
        }
        return binding.root
    }

    private fun showBottomSheet() {
        val bottomSheet = BottomSheetDialog(requireContext())
        val viewBinding = BottomSheetStartQuizBinding.inflate(
            layoutInflater, binding.container, false
        )

        viewBinding.btnStart.setOnClickListener {
            val name = viewBinding.etName.text.toString()
            if (name.isNotEmpty()) {
                bottomSheet.dismiss()
                this.findNavController().navigate(
                    QuizIntroFragmentDirections.actionIntoToQuiz(name)
                )
            } else {
                Toast.makeText(requireContext(), "Nama wajib diisi", Toast.LENGTH_SHORT).show()
            }
        }
        bottomSheet.setContentView(viewBinding.root)
        bottomSheet.show()
    }
}