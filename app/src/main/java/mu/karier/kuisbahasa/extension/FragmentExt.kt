package mu.karier.kuisbahasa.extension

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment

fun Fragment.openChromeBrowser(url: String) {
    if (isChromeInstalled()) {
        val builder = CustomTabsIntent.Builder()
        builder.setShowTitle(true)
        builder.enableUrlBarHiding()
        try {
            startActivityForResult(builder.build().intent.setData(Uri.parse(url)), 911)
        } catch (e: Exception) {
            openAppBrowser(url)
        }
    } else {
        openAppBrowser(url)
    }
}

fun Fragment.openAppBrowser(url: String) {
    try {
        var direct = url
        if (!url.startsWith("http://") && !url.startsWith("https://")) direct = "http://$url"
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(direct))
        requireContext().startActivity(browserIntent)
    } catch (e: Exception) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/apps/details?id=com.android.chrome")
        )
        startActivity(intent, null)
    }
}

fun Fragment.isChromeInstalled(): Boolean {
    return try {
        val pInfo = requireContext().packageManager.getPackageInfo("com.android.chrome", 0)
        pInfo != null
    } catch (e: PackageManager.NameNotFoundException) {
        //chrome is not installed on the device
        false
    }
}