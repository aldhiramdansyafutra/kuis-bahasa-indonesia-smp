package mu.karier.kuisbahasa

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import mu.karier.kuisbahasa.database.DatabaseClient
import mu.karier.kuisbahasa.database.entity.LeaderboardRepository

class Application: Application() {

    val applicationScope = CoroutineScope(SupervisorJob())
    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database by lazy { DatabaseClient.getDatabase(this, applicationScope) }
    val repository by lazy { LeaderboardRepository(database.leaderBoard()) }
}