package mu.karier.kuisbahasa.database.entity

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class LeaderboardRepository constructor(private val leaderboardDao: LeaderboardDao) {

    var mAllLeaderboard: Flow<List<LeaderboardEntity>> = leaderboardDao.findAll()

    @WorkerThread
    suspend fun insert(entity: LeaderboardEntity) {
        leaderboardDao.insert(entity)
    }
}