package mu.karier.kuisbahasa.database.entity

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface LeaderboardDao {
    @Query("SELECT * from tbl_leaderboard ORDER BY score DESC")
    fun findAll(): Flow<List<LeaderboardEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(newsCache: LeaderboardEntity)

    @Query("DELETE FROM tbl_leaderboard")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(favorite: LeaderboardEntity)
}