package mu.karier.kuisbahasa.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import mu.karier.kuisbahasa.database.entity.LeaderboardDao
import mu.karier.kuisbahasa.database.entity.LeaderboardEntity
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Database(
    entities = [LeaderboardEntity::class],
    version = 1,
    exportSchema = false
)
abstract class DatabaseClient : RoomDatabase() {

    abstract fun leaderBoard(): LeaderboardDao

    companion object {

        @Volatile
        private var INSTANCE: DatabaseClient? = null

        fun getDatabase(context: Context, scope: CoroutineScope): DatabaseClient {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DatabaseClient::class.java,
                    "db_kuis_sederhana"
                ).addCallback(DatabaseCallback(scope)).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

    private class DatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    val dao = database.leaderBoard()

                    // Delete all content here.
                    dao.deleteAll()

                    // Add sample words.
                    dao.insert(LeaderboardEntity(username = "John Doe", score = 70))
                }
            }
        }
    }
}