package mu.karier.kuisbahasa.data

import android.content.Context
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

data class Materi(
    val name: String = "",
    val url: String = "",
    val description: String = ""
) {
    companion object {
        fun getMateri(context: Context): ArrayList<Materi> {
            val list: ArrayList<Materi> = ArrayList()
            try {
                val obj = JSONObject(loadLocationJson(context))
                val data: JSONArray = obj.getJSONArray("data")
                val count = data.length()
                for (i in 0 until count) {
                    list.add(parse(data.getJSONObject(i)))
                }
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
            list.shuffle()
            return list
        }

        private fun loadLocationJson(context: Context): String {
            var json = ""
            return try {
                val `is`: InputStream = context.assets.open("materi.json")
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, Charset.forName("UTF-8"))
                json
            } catch (ex: IOException) {
                ex.printStackTrace()
                json
            }
        }

        private fun parse(location: JSONObject): Materi {
            return Materi(
                description = location.getString("deskripsi"),
                name = location.getString("nama"),
                url = location.getString("url")
            )
        }
    }
}