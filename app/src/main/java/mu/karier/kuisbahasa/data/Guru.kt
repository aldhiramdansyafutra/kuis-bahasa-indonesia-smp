package mu.karier.kuisbahasa.data

import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset


@Parcelize
data class Guru(
    val nama: String,
    val phone: String,
    val studi: String,
    val nip: String,
    val avatar: String
) : Parcelable {
    companion object {
        fun getGuru(context: Context): ArrayList<Guru> {
            val list: ArrayList<Guru> = ArrayList()
            try {
                val obj = JSONObject(loadLocationJson(context))
                val data: JSONArray = obj.getJSONArray("data")
                val count = data.length()
                for (i in 0 until count) {
                    list.add(parse(data.getJSONObject(i)))
                }
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
            return list
        }

        private fun loadLocationJson(context: Context): String {
            var json = ""
            return try {
                val `is`: InputStream = context.assets.open("guru.json")
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, Charset.forName("UTF-8"))
                json
            } catch (ex: IOException) {
                ex.printStackTrace()
                json
            }
        }

        private fun parse(location: JSONObject): Guru {
            return Guru(
                nama = location.getString("nama"),
                studi = location.getString("studi"),
                phone = location.getString("telp"),
                nip = location.getString("nip"),
                avatar = location.getString("avatar")
            )
        }
    }
}